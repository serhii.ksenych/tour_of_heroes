import { Injectable } from '@angular/core';
import { Hero } from './hero';
import { Observable, of } from 'rxjs';
import { HEROES } from './mock-herous';
import { MessagesService } from './messages.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class HeroService
{
    httpOptions = {
        headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    };
    private heroesUrl = 'api/heroes';
    heroes: Hero[];
    constructor(
        private messagesService: MessagesService,
        private httpClient: HttpClient) { }

    getHeroes(): Observable<Hero[]>
    {
        // this.messagesService.addMessage('HeroService: fetched heroes');
        return this.httpClient.get<Hero[]>(this.heroesUrl).pipe
            (
                tap(_=>this.log('fetched heroes')),
                catchError(this.handleError<Hero[]>('getHeroes', []))
            );
    }

    getHero(id: number): Observable<Hero>
    {
        const url = `${this.heroesUrl}/${id}`;
        return this.httpClient.get<Hero>(url).pipe
            (
                tap(_ => this.log(`fetched hero id=${id}`)),
                catchError(this.handleError<Hero>(`getHero id=${id}`))
            );
    }

    private log(message: string)
    {
        this.messagesService.addMessage(`HeroService: ${message}`);
    }

    private handleError<T>(operation = 'operation', result?: T)
    {
        return (error: any): Observable<T> =>
        {
            console.log(error);
            this.log(`${operation} failed: ${error.message}`);
            return of(result as T);
        }
    }

    updateHero(hero: Hero): Observable<any>
    {
        return this.httpClient.put(this.heroesUrl, hero, this.httpOptions).pipe
            (
                tap(_ => this.log(`updated hero id=${hero.id}`)),
                catchError(this.handleError<any>('updatedHero'))
            );
    }

    addHero(hero: Hero): Observable<Hero>
    {
        return this.httpClient.post<Hero>(this.heroesUrl, hero, this.httpOptions).pipe
            (
                tap((newHero: Hero) => this.log(`added hero w/ id=${newHero.id}`)),
                catchError(this.handleError<Hero>('addHero'))
            );
    }

    deleteHero(hero: Hero | number): Observable<Hero>
    {
        const id = typeof hero === 'number' ? hero : hero.id;
        const url = `${this.heroesUrl}/${id}`;

        return this.httpClient.delete<Hero>(url, this.httpOptions).pipe
            (
                tap(_ => this.log(`deleted hero id=${id}`)),
                catchError(this.handleError<Hero>('deleteHero'))
            );
    }

    searchHeroes(term: string): Observable<Hero[]>
    {
        const url = `${this.heroesUrl}/?name=${term}`;
        if (!term.trim())
            return of([]);
        return this.httpClient.get<Hero[]>(url).pipe(
                tap(x => x.length ?
                    this.log(`found heroes matching "${term}"`):
                    this.log(`no heroes matching "${term}"`),
                catchError(this.handleError<Hero[]>('searchHeroes',[]))
        )); 
    }
}

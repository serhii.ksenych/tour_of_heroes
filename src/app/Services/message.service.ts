import { Injectable, EventEmitter } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class MessageService
{
  messagesChanged: EventEmitter<any> = new EventEmitter();
  messages: string[] = [];
  constructor() { }

  add(message: string): void
  {
    this.messages.push(message);
    this.messagesChanged.emit();
  }

  clear(): void
  {
    this.messages = [];
  }
}

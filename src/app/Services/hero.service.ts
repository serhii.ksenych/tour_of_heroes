import { Injectable } from '@angular/core';
import { Hero } from '../models/hero';
import { HEROES } from '../models/mock-heroes';
import { Observable, of } from 'rxjs';
import { MessageService } from './message.service';

@Injectable({
  providedIn: 'root'
})
export class HeroService
{
  counter: number=1;
  constructor(private messageService: MessageService) { }

  getHeroes(): Observable<Hero[]>
  {
    this.messageService.add(this.counter + ': HeroService:fetched heroes');
    this.counter++;
    return of(HEROES);
  }

  getHero(id: number): Observable<Hero>
  {
    let hero = HEROES.find(h => h.id == id);
    return of(hero);
  }
}
